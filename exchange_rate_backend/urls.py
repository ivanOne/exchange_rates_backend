from django.urls import path, include

from exchange_api.views import CurrencyListView, RateView

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path('currencies', CurrencyListView.as_view()),
    path('rate/<int:usd_id>', RateView.as_view())
]
