### Тестовое задание

Задача.

Необходимо реализовать сервис курсов валют на Руthon З.6+


**Технологии для решения**
- python
- django
- django rest framework
- requests

**Как запустить**

1. Необходимо окружение с python версии 3.6.*, и пакетный менеджер pip.
2. В корневой директории проекта нужно запустить pip install -r requirements.txt
3. В директории exchange_api требуется создать файл local_settings.py и
    указать в нем настройки соединений с базой данных:
```python
DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'db_name',
            'USER': 'user',
            'PASSWORD': 'password',
            'HOST': 'localhost',
            'PORT': '',
        }
    }
```
4. Запустить миграции, из корневой директории **python manage.py migrate**

Для за запуска тестов нужно использовать **python manage.py test**

Запуск тестового сервера **python manage.py runserver**

Создать пользовалетя с правами администратора **python manage.py createsuperuser**

Сбор данных с Bitfinex **python manage.py collect_exchange_rates**

Можно использовать browsable api.