import datetime

import requests
from django.conf import settings
from django.core.management import BaseCommand

from exchange_api.models import Currency, Rate


class Command(BaseCommand):
    help = 'Collect exchange rates from Bitfinex'

    def handle(self, *args, **options):
        currency_db_map = {}
        currency_for_collect = settings.CURRENCY_LIST_FOR_COLLECT

        for currency_name in currency_for_collect:
            currency, created = Currency.objects.get_or_create(name=currency_name)
            currency_db_map.update({f'{currency_name}USD': currency.pk})

        datetime_stamp_index = 0
        close_index = 2
        volume_index = 5
        day_range = 10
        rates_rows = []

        for symbol, currency_db_pk in currency_db_map.items():
            response = requests.get(f'https://api-pub.bitfinex.com/v2/candles/trade:1D:t{symbol}/hist')
            for exchange_day_info in response.json()[:day_range]:
                exchange_date = datetime.datetime\
                    .utcfromtimestamp(exchange_day_info[datetime_stamp_index] / 1000).date()
                rates_rows.append(Rate(currency_id=currency_db_pk, date=exchange_date,
                                       rate=exchange_day_info[close_index],
                                       volume=exchange_day_info[volume_index]))

        Rate.objects.bulk_create(rates_rows, batch_size=20, ignore_conflicts=True)
