import datetime
import random

import vcr
from django.contrib.auth.models import User
from django.core.management import call_command
from django.db.models import Avg
from django.test import TestCase, override_settings
from rest_framework import status
from rest_framework.test import APIClient

from exchange_api.models import Currency, Rate


@override_settings(CURRENCY_LIST_FOR_COLLECT=['BTC', 'ETH', 'AGI'])
class TestCollectExchangeRates(TestCase):

    def test_collect_ok(self):
        with vcr.use_cassette('fixtures/vcr_cassettes/bitfinex_cassette.yaml') as cassette:
            call_command('collect_exchange_rates')
            self.assertEqual(len(cassette.requests), 3)

        self.assertEqual(Currency.objects.all().count(), 3)
        self.assertEqual(Rate.objects.all().count(), 30)

    def test_double_run_command(self):
        with vcr.use_cassette('fixtures/vcr_cassettes/bitfinex_cassette.yaml') as cassette:
            call_command('collect_exchange_rates')
            self.assertEqual(len(cassette.requests), 3)
        with vcr.use_cassette('fixtures/vcr_cassettes/bitfinex_cassette.yaml') as cassette:
            call_command('collect_exchange_rates')
            self.assertEqual(len(cassette.requests), 3)

        self.assertEqual(Currency.objects.all().count(), 3)
        self.assertEqual(Rate.objects.all().count(), 30)


class CurrencyListApiTestCase(TestCase):
    client_class = APIClient

    def setUp(self):
        self.user = User.objects.create_user('user', 'admin@admin.com', '123')
        self.currency_list = ['BTC', 'ETH', 'AGI', 'AID', 'IOT', 'AMP', 'ATO', 'BAB', 'TSD', 'CSX', 'CTX']
        currency_for_create = []
        for currency in self.currency_list:
            currency_for_create.append(Currency(name=currency))
        Currency.objects.bulk_create(currency_for_create)

    def test_ok(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get('/currencies')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 11)
        self.assertEqual(response.data['next'], 'http://testserver/currencies?page=2')
        self.assertIsNone(response.data['previous'])
        self.assertEqual(len(response.data['results']), 10)
        self.assertListEqual(response.json()['results'], list(Currency.objects.all().values('id', 'name')[:10]))

    def test_pagination_ok(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get('/currencies?page=2')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 11)
        self.assertEqual(response.data['previous'], 'http://testserver/currencies')
        self.assertIsNone(response.data['next'])
        self.assertEqual(len(response.data['results']), 1)
        self.assertListEqual(response.json()['results'], list(Currency.objects.all().values('id', 'name')[10:11]))

    def test_no_auth(self):
        response = self.client.get('/currencies')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_currency_empty(self):
        Currency.objects.all().delete()
        self.client.force_authenticate(user=self.user)
        response = self.client.get('/currencies')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 0)
        self.assertIsNone(response.data['previous'])
        self.assertIsNone(response.data['next'])
        self.assertEqual(len(response.data['results']), 0)
        self.assertListEqual(response.json()['results'], [])


class TestRateApiTestCase(TestCase):
    client_class = APIClient

    def setUp(self):
        self.user = User.objects.create_user('user', 'admin@admin.com', '123')
        self.currency_btc = Currency.objects.create(name='BTC')
        start = datetime.datetime.today()
        date_list = [start - datetime.timedelta(days=x) for x in range(10)]

        for date_item in date_list:
            rate = round(random.uniform(1, 50), 5)
            volume = round(random.uniform(1, 5000), 4)
            Rate.objects.create(date=date_item.date(), rate=rate, volume=volume, currency=self.currency_btc)

    def test_ok(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(f'/rate/{self.currency_btc.pk}')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['last_rate'], Rate.objects.order_by('-date').first().rate)
        self.assertEqual(response.json()['avg_volume'],
                         Rate.objects.all().aggregate(volume_avg=Avg('volume'))['volume_avg'])

    def test_trading_volume_for_ten_days(self):
        expected_avg = Rate.objects.all().aggregate(volume_avg=Avg('volume'))['volume_avg']
        eleven_day = datetime.datetime.now() - datetime.timedelta(days=11)
        Rate.objects.create(date=eleven_day.date(), rate=50.5000, volume=3000.8955, currency=self.currency_btc)

        self.client.force_authenticate(user=self.user)
        response = self.client.get(f'/rate/{self.currency_btc.pk}')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['last_rate'], Rate.objects.order_by('-date').first().rate)
        self.assertEqual(response.json()['avg_volume'], expected_avg)

    def test_no_rates(self):
        Rate.objects.all().delete()
        self.client.force_authenticate(user=self.user)
        response = self.client.get(f'/rate/{self.currency_btc.pk}')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['last_rate'], None)
        self.assertEqual(response.json()['avg_volume'], 0)

    def test_currency_not_exists(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(f'/rate/0')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_not_auth(self):
        response = self.client.get('/currencies')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
