from django.db import models


class Currency(models.Model):
    name = models.CharField('Currency name', max_length=3)

    class Meta:
        ordering = ['id']


class Rate(models.Model):
    currency = models.ForeignKey(Currency, verbose_name='Currency', on_delete=models.CASCADE,
                                 related_name='currency_rates')
    date = models.DateField('Date')
    rate = models.FloatField('Rate')
    volume = models.FloatField('Volume')

    class Meta:
        unique_together = ('currency', 'date')

