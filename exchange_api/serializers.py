from datetime import timedelta

from django.db.models import Avg
from django.db.models.functions import Coalesce
from django.utils.timezone import now
from rest_framework import serializers

from exchange_api.models import Currency, Rate


class CurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = Currency
        fields = ('id', 'name')


class CurrencyRateSerializer(serializers.ModelSerializer):
    last_rate = serializers.SerializerMethodField()
    avg_volume = serializers.SerializerMethodField()

    def get_last_rate(self, currency):
        return Rate.objects.filter(currency=currency).order_by('-date').values_list('rate', flat=True).first()

    def get_avg_volume(self, currency):
        # Average trading volume for 10 days
        now_datetime = now() - timedelta(days=10)
        return Rate.objects\
            .filter(currency=currency, date__gte=now_datetime.date())\
            .aggregate(avg_volume=Coalesce(Avg('volume'), 0))['avg_volume']

    class Meta:
        model = Currency
        fields = ('last_rate', 'avg_volume')
