from rest_framework import permissions, status
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from exchange_api.models import Currency
from exchange_api.serializers import CurrencySerializer, CurrencyRateSerializer


class CurrencyListView(ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    model = Currency
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer


class RateView(APIView):
    permissions_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        currency = get_object_or_404(Currency, pk=kwargs.get('usd_id', 0))
        serializer = CurrencyRateSerializer(currency)
        return Response(serializer.data, status=status.HTTP_200_OK)
