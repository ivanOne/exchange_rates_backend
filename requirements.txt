django==2.2.5
psycopg2==2.8.3
requests==2.22.0
djangorestframework==3.10.3
markdown==3.1.1
vcrpy==2.1.0